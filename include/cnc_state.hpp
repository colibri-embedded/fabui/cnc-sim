#ifndef CNC_STATE_HPP
#define CNC_STATE_HPP

#define MAX_NOZZLE_COUNT 4

namespace fabui {

enum class PositionMode {
    ABSOLUTE,
    RELATIVE
};

struct CNCState {
    float min_x;
    float max_x;
    float min_y;
    float max_y;
    float min_z;
    float max_z;

    float x;
    float y;
    float z;
    float e;

    float feedrate;

    PositionMode xyz_mode;
    PositionMode e_mode;

    float nozzle_temp[MAX_NOZZLE_COUNT];
    float nozzle_target_temp[MAX_NOZZLE_COUNT];
    float bed_temp;
    float bed_target_temp;

    CNCState();
};

} // namespace fabui

#endif /* CNC_STATE_HPP */
