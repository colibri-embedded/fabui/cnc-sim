/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_parser.hpp
 * @brief GCode parser definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef GCODE_PARSER_HPP
#define GCODE_PARSER_HPP

#include <regex>
#include <string>
#include <cstdint>
#include <map>
#include "gcode.hpp"

namespace fabui {

class GCodeParser {
	public:
		GCodeParser();

		GCode parse(const std::string& gcode_line);
		uint32_t hashGcode(const std::string& gcode);
	private:
		std::regex m_segment_re;
};

} // namespace fabui

#endif /* GCODE_PARSER_HPP */
