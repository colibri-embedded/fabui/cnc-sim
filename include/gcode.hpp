/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode.hpp
 * @brief GCode command definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef GCODE_HPP
#define GCODE_HPP

#include <string>
#include <map>

namespace fabui {

/// GCode command.
/**
 * GCode class holds the parsed parameters of a GCode command.
 */
struct GCode {
	/// GCode command (only)
	std::string code;
	/// Hash value of the gcode command
	uint32_t    hash;
	/// GCode arguments
	std::map<char, std::string> enval;

	/**
	 * Check if comman has an argument with name label
	 *
	 * @param label Argument label
	 *
	 * @returns true if argument exists, false otherwise
	 */
	bool 	have(const char& label);

	/**
	 * Get a int value representation of an argument.
	 *
	 * @param label Argument label
	 * @returns Argument value as int
	 */
	int 	getInt(const char& label);

	/**
	 * Set argument to an int value.
	 *
	 * @param label Argument label
	 * @param value Argument value
	 */
	void setInt(const char& label, int value);

	/**
	 * Get a float value representation of an argument.
	 *
	 * @param label Argument label
	 * @returns Argument value as float
	 */
	float 	getFloat(const char& label);

	/**
	 * Set argument to an int value.
	 *
	 * @param label Argument label
	 * @param value Argument value
	 */
	void setFloat(const char& label, float value);

	/**
	 * Get a bool value representation of an argument.
	 *
	 * @param label Argument label
	 * @returns Argument value as bool
	 */
	bool 	getBool(const char& label);

	/**
	 * Set argument to an bool value.
	 *
	 * @param label Argument label
	 * @param value Argument value
	 */
	void setBool(const char& label, bool value);

	/**
	 * Compare with a gcode command name
	 *
	 * @returns true if equal, false otherwise
	 */
	bool 	operator==(const std::string&) const;

	/**
	 * Check if not equal to gcode command name
	 *
	 * @returns true if equal, false otherwise
	 */
	bool 	operator!=(const std::string&) const;

	/**
	 * Compare if  a hash value
	 *
	 * @returns true if equal, false otherwise
	 */
	bool 	operator==(const uint32_t) const;

	/**
	 * Compare it not equal to hash value
	 *
	 * @returns true if equal, false otherwise
	 */
	bool 	operator!=(const uint32_t) const;

	/**
	 * Convert current gcode object to string.
	 */
	std::string to_string(bool compact=false, unsigned precision=5);
};

} // namespace fabui

#endif /* GCODE_HPP */