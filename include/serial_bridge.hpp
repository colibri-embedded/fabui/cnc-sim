#ifndef SERIAL_UTILS_HPP
#define SERIAL_UTILS_HPP

#include <string>

class SerialBridge {
	public:
		SerialBridge(const std::string& port0, const std::string& port1);
		virtual ~SerialBridge();

		bool createSerialPorts();
		void closeSerialPorts();
	private:
		std::string m_port0;
		std::string m_port1;
};

#endif /* SERIAL_UTILS_HPP */
