#ifndef CNC_SIM_HPP    //
#define CNC_SIM_HPP

#include <string>
#include <thread>
#include <serialxx/thread_safe.hpp>

#include <fabui/thread/event.hpp>
#include <fabui/thread/queue.hpp>

#include "gcode_parser.hpp"
#include "cnc_state.hpp"

namespace fabui {

class CNCSimulator
{
public:
    CNCSimulator(
        const std::string& format,
        const std::string& flavour,
        const std::string& portName,
        unsigned baudRate = 115200,
        unsigned timeout = 1000);

    ~CNCSimulator();

    void run();
    void stop();

protected:
    serialxx::thread_safe::Serial m_serial;
    unsigned m_timeout;
    std::string m_format;
    std::string m_flavour;

    std::string m_line_end;
    GCodeParser m_parser;

    std::mutex m_state_mut;
    CNCState m_state;

    std::mutex m_running_mut;
    bool m_running;

    Queue<std::string, 1> txq;
    Queue<std::string, 8> rxq;

    void start();


    void writeLine(const std::string& line);
    // bool send(const std::string& line, bool blocking=true);

    Event m_receiver_thread_started;
    std::thread m_receiver_thread;
    void receiverThreadLoop();

    Event m_worker_thread_started;
    std::thread m_worker_thread;
    void workerThreadLoop();
    void processCommand(const std::string& line);

    // void processReplyLine(const std::string& line);

    void setNewPosition(float x, float y, float z, float e, float feedrate);
    void setNozzleTargetTemperature(float temp, unsigned index=0);
    void setBedTemperature(float temp);
};

} // namespace fabui

#endif /* CNC_SIM_HPP */
