/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode.cpp
 * @brief GCode command implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "gcode.hpp"
#include <iomanip>

using namespace fabui;

bool GCode::operator==(const std::string& _code) const {
	return code == _code;
}

bool GCode::operator==(const uint32_t _hash) const {
	return hash == _hash;
}

bool GCode::operator!=(const std::string& _code) const {
	return code != _code;
}

bool GCode::operator!=(const uint32_t _hash) const {
	return hash != _hash;
}

bool GCode::have(const char& label) {
	return enval.count(label);
}

int GCode::getInt(const char& label) {
	if(have(label)) {
		auto value = enval[label];
		return std::stoi(value);
	} else {
		throw std::invalid_argument("Label does not exist");
	}
	return 0;
}

void GCode::setInt(const char& label, int value) {
	if(have(label)) {
		enval[label] = std::to_string(value);
	} else {
		throw std::invalid_argument("Label does not exist");
	}
}

float GCode::getFloat(const char& label) {
	if(have(label)) {
		auto value = enval[label];
		return std::stof(value);
	} else {
		throw std::invalid_argument("Label does not exist");
	}
	return 0.0f;
}

void GCode::setFloat(const char& label, float value) {
	if(have(label)) {
		enval[label] = std::to_string(value);
	} else {
		throw std::invalid_argument("Label does not exist");
	}
}

bool GCode::getBool(const char& label) {
	if(have(label)) {
		auto value = enval[label];
		return std::stoi(value);
	} else {
		throw std::invalid_argument("Label does not exist");
	}
	return false;
}

void GCode::setBool(const char& label, bool value) {
	if(have(label)) {
		enval[label] = std::to_string(value);
	} else {
		throw std::invalid_argument("Label does not exist");
	}
}

std::string GCode::to_string(bool compact, unsigned precision) {
	std::string gcode;

	gcode += code;

	for(auto &kv : enval) {
		if(kv.first == 'G' or kv.first == 'M')
			continue;

		if(not compact)
			gcode += " ";

		gcode += kv.first;

		std::ostringstream streamObj3;
		// Set Fixed -Point Notation
		streamObj3 << std::fixed;
		// Set precision to 2 digits
		streamObj3 << std::setprecision(precision);
		//Add double to stream
		streamObj3 << std::stof(kv.second);
		// Get string from output string stream
		std::string val = streamObj3.str();
		// Remove trailing zeros
		val.erase ( val.find_last_not_of('0') + 1, std::string::npos);
		val.erase ( val.find_last_not_of('.') +1, std::string::npos);
		gcode += val;

	}

	return gcode;
}