#include "config.h"
#include <CLI/CLI.hpp>
#include <iostream>
#include <string>
#include <csignal>

#include "cnc_sim.hpp"
#include "serial_bridge.hpp"

#define CNC_PORT "/tmp/ttyCNC0"

void signal_handler(int param) {
    std::cout << "interrupt: " << param << std::endl;
}

void show_version(size_t)
{
    std::cout << PACKAGE_STRING << std::endl;
    throw CLI::Success();
}

static fabui::CNCSimulator *p_cnc = nullptr;

int main(int argc, char* argv[])
{
    CLI::App app { PACKAGE_NAME };

    std::string format = "gcode";
    std::string flavour = "marlin-1.1.x";
    std::string portName = "/dev/ttyS0";
    unsigned baudRate = 115200;
    bool virtual_port = false;

    app.add_option("-f,--format", format, "Command format [default: " + format + "]");
    app.add_option("-l,--flavour", flavour, "Format flavour [default: " + flavour + "]");
    app.add_flag("--virtual", virtual_port, "Create a virtual port");
    app.add_option("-p,--port", portName, "Serial port [default: " + portName + "]");
    app.add_option("-b,--baud", baudRate, "Serial baud rate [default: " + std::to_string(baudRate) + "]");
    app.add_config("-c,--config", "config.ini", "Read config from an ini file", false);
    app.add_flag_function("-v,--version", show_version, "Show version");

    try {
        app.parse(argc, argv);
    } catch (const CLI::ParseError& e) {
        return app.exit(e);
    }

    if(virtual_port) {
        std::cout << "virtual port\n";
        SerialBridge sb(CNC_PORT, portName);
        if( !sb.createSerialPorts() ) {
            return 1;
        }

        fabui::CNCSimulator cnc(format, flavour, CNC_PORT, baudRate);
        p_cnc = &cnc;
        signal(SIGINT, [](int){
            std::cout << "CTRL+C interrupt\n";
            p_cnc->stop();
        });
        cnc.run();

        sb.closeSerialPorts();
    } else {
        fabui::CNCSimulator cnc(format, flavour, portName, baudRate);
        p_cnc = &cnc;
        signal(SIGINT, [](int){
            std::cout << "CTRL+C interrupt\n";
            p_cnc->stop();
        });
        cnc.run();
    }

    return 0;
}
