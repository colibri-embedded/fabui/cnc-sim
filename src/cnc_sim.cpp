#include "cnc_sim.hpp"
#include "gcode_hashed.hpp"
#include <iostream>
#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fabui/utils/string.hpp>

#define READ_SHARED(var_name) { std::lock_guard<std::mutex>  lock(m_##var_name##_mut); var_name = m_##var_name; }
#define UPDATE_SHARED(var_name) { std::lock_guard<std::mutex>  lock(m_##var_name##_mut); m_##var_name = var_name; }
#define WRITE_SHARED(var_name, value) { std::lock_guard<std::mutex>  lock(m_##var_name##_mut); m_##var_name = (value); }
#define GUARD_SHARED(var_name)  std::lock_guard<std::mutex> guard(var_name##_mut)

using namespace fabui;

CNCSimulator::CNCSimulator(const std::string& format, const std::string& flavour,
    const std::string& portName, unsigned baudRate, unsigned timeout)
    : m_timeout(timeout)
    , m_format(format)
    , m_flavour(flavour)
    , m_line_end("\n")
    , m_running(false)
{
    m_serial.setPortName(portName);
    m_serial.setBaudRate(baudRate);
    m_serial.setTimeout(timeout);
}

CNCSimulator::~CNCSimulator()
{
    stop();
}

void CNCSimulator::start()
{
    m_serial.open();
    {
        GUARD_SHARED(m_running);
        if (m_running)
            return;
        m_running = true;
    }

    m_receiver_thread = std::thread(&CNCSimulator::receiverThreadLoop, this);
    m_worker_thread = std::thread(&CNCSimulator::workerThreadLoop, this);

    m_receiver_thread_started.wait();
    m_worker_thread_started.wait();
}

void CNCSimulator::stop()
{
    {
        GUARD_SHARED(m_running);
        if (not m_running)
            return;
        m_running = false;
    }

    rxq.push("");
    m_serial.abort();
}

void CNCSimulator::run()
{
    std::cout << "starting..." << "\n";
    start();
    std::cout << "started" << "\n";

    if(m_receiver_thread.joinable())
        m_receiver_thread.join();
    if(m_worker_thread.joinable())
        m_worker_thread.join();

    std::cout << "finished" << "\n";
    m_serial.close();
}

void CNCSimulator::setNewPosition(float x, float y, float z, float e, float feedrate)
{
    GUARD_SHARED(m_state);
    if(m_state.xyz_mode == PositionMode::ABSOLUTE) {
        m_state.x = x;
        m_state.y = y;
        m_state.z = z;
    } else {
        m_state.x += x;
        m_state.y += y;
        m_state.z += z;
    }
    if(m_state.e_mode == PositionMode::ABSOLUTE) {
        m_state.e = e;
    } else {
        m_state.e += e;
    }
    m_state.feedrate = feedrate;
}

void CNCSimulator::setBedTemperature(float temp)
{
    GUARD_SHARED(m_state);

}

void CNCSimulator::setNozzleTargetTemperature(float temp, unsigned index)
{
}

void CNCSimulator::processCommand(const std::string& line)
{
    auto gcode = m_parser.parse(line);
    CNCState state;
    READ_SHARED(state);

    switch(gcode.hash) {
        case G0:
        case G1:
            {
                float x,y,z,e;
                x = y = z = e = 0.0;
                if(state.xyz_mode == PositionMode::ABSOLUTE) {
                    x = state.x;
                    y = state.y;
                    z = state.z;
                }
                if(state.e_mode == PositionMode::ABSOLUTE) {
                    e = state.e;
                }
                float feedrate = state.feedrate;
                if(gcode.have('F')) {
                    feedrate = gcode.getInt('F');
                }
                setNewPosition(x, y, z, e, feedrate);
            }
            break;
        case M104:
            {
                if(gcode.have('S')) {
                    float new_temp = gcode.getFloat('S');
                    unsigned tidx = 0;
                    if(gcode.have('T')) {
                        tidx = gcode.getInt('T');
                    }
                    if(tidx < MAX_NOZZLE_COUNT) {
                        setNozzleTargetTemperature(new_temp, tidx);
                    }
                }
            }
            break;
        case M140:
            {

            }
            break;
        case M109:
            {

            }
            break;
        case M190:
            {

            }
            break;
        case G28:
            {
                std::cout << "G28" << std::endl;
                bool have_x = gcode.have('X');
                bool have_y = gcode.have('Y');
                bool have_z = gcode.have('Z');
                bool have_all = !have_x && !have_y && !have_z;
                if(have_x or have_all)
                    state.x = 0;
                if(have_y or have_all)
                    state.y = 0;
                if(have_z or have_all)
                    state.z = 0;
                UPDATE_SHARED(state);
            }
            break;
        case M105:
            {

            }
            break;
        case M114:
            {
                std::cout << "M114" << std::endl;
                writeLine(fmt::format("[{}, {}, {}]", state.x, state.y, state.z));
            }
        default:;
    }

    writeLine("ok");
}

void CNCSimulator::workerThreadLoop()
{
    m_worker_thread_started.set();
    bool running = true;

    while(true) {
        READ_SHARED(running);
        if(not running)
            break;

        std::string line;
		rxq.pop(line);

		if(not line.empty()) {
            processCommand(line);
        }
    }
}

void CNCSimulator::receiverThreadLoop()
{
	m_receiver_thread_started.set();
	bool running = true;

	std::string prev_fragment;

	while(true) {
		READ_SHARED(running);

		if(not running) {
			break;
		}

		try {
			std::string data;
			if( m_serial.readLine(data, m_timeout) ) {

				bool save_last = false;
				if( data.c_str()[data.length()-1] != '\n' ) {
					save_last = true;
				}

				auto lines = utils::str::split(data, '\n');
				unsigned len = lines.size();
				for(unsigned i=0; i<len; i++) {
					auto &line = lines[i];
					if(save_last and (i+1) == len) {
						prev_fragment = line;
					} else {
						if(!prev_fragment.empty()) {
							prev_fragment += line;
                            rxq.try_push(prev_fragment);
							prev_fragment.clear();
						} else {
                            rxq.try_push(prev_fragment);
						}

					}
				}
			}

		} catch(...) {
			//
		}
	};
}

void CNCSimulator::writeLine(const std::string& line)
{
	m_serial.write(line + m_line_end);
}
