/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_parser.cpp
 * @brief GCode parser implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "gcode_parser.hpp"
#include "gcode_hashed.hpp"
#include <iostream>

using namespace fabui;

GCodeParser::GCodeParser() 
	: m_segment_re("([A-Z])([-+]?[0-9.]+)")
{

}

GCode GCodeParser::parse(const std::string& gcode_line) {
	GCode gcode;
	bool have_command = false;
	auto s = gcode_line;
	std::smatch m;

	gcode.hash = 0;

	while(std::regex_search (s, m, m_segment_re)) {
		if(m.size()) {
			std::string slabel = m[0];
			char label = slabel[0];

			gcode.enval[label] = m[2];

			if(not have_command) {
				if(label == 'G' or label == 'M') {
					gcode.code = slabel;
					gcode.hash = hashGcode(slabel);
					have_command = true;
				}
			}
		}
		
		s = m.suffix().str();
	}

	// std::cout << gcode_line << " HASH: " << gcode.hash << " as " << gcode.code << std::endl;

	if(gcode.hash < 0xff) {
		throw std::invalid_argument("Invalid gcode");
	}

	return std::move(gcode);
}

uint32_t GCodeParser::hashGcode(const std::string& gcode) {
    auto len = gcode.size();
    uint32_t result = 0;
    if(len > 0)
        result |= gcode[0];
    if(len > 1)
        result |= gcode[1] << 8;
    if(len > 2)
        result |= gcode[2] << 16;
    if(len > 3)
        result |= gcode[3] << 24;
    return result;
}