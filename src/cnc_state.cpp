#include "cnc_state.hpp"

using namespace fabui;

CNCState::CNCState() {
    xyz_mode = PositionMode::ABSOLUTE;
    e_mode = PositionMode::ABSOLUTE;

    e = x = y = z = 0.0f;
    feedrate = 1000.0f;

    min_x = min_y = min_z = 0.0f;
    max_x = max_y = max_z = 25.0f;

    nozzle_temp[0] = nozzle_temp[1] = nozzle_temp[2] = nozzle_temp[3] = 21.0f;
    nozzle_target_temp[0] = nozzle_target_temp[1] = nozzle_target_temp[2] = nozzle_target_temp[3] = 0.0f;

    bed_temp = 0.0f;
    bed_target_temp = 0.0f;
}
