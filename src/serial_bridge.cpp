#include <errno.h>
#include <unistd.h>
#include <libgen.h>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <iostream>
#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include "serial_bridge.hpp"

using namespace std::literals;

SerialBridge::SerialBridge(const std::string& port0, const std::string& port1)
	: m_port0(port0)
	, m_port1(port1)
{

}

SerialBridge::~SerialBridge()
{
	closeSerialPorts();
}

bool SerialBridge::createSerialPorts()
{
	char *port0dup = strdup( m_port0.c_str() );
	char *port0dir = dirname( port0dup );
	if(access(port0dir, W_OK) != 0) {
		std::cout << "cannot write to " << port0dir << std::endl;
		free(port0dup);
		return false;
	}
	free(port0dup);

	char *port1dup = strdup( m_port1.c_str() );
	char *port1dir = dirname( port1dup );
	if(access(port1dir, W_OK) != 0) {
		std::cout << "cannot write to " << port0dir << std::endl;
		free(port1dup);
		return false;
	}
	free(port1dup);
	// Make sure no other socat instance is running
	std::system("pkill socat");
	std::string cmd = fmt::format("socat -d -d pty,raw,echo=0,link={} pty,raw,echo=0,link={} > /dev/null 2>&1 &", m_port0, m_port1);
	std::system(cmd.c_str());
	std::this_thread::sleep_for(1s);

	return true;
}

void SerialBridge::closeSerialPorts()
{
	std::system("pkill socat");
}
